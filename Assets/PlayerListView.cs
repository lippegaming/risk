﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListView : Photon.PunBehaviour
{
    public UnityEngine.UI.VerticalLayoutGroup verticalLayoutGroup;
    public Text PlayerTextPrefab;

    private RectTransform _parent;
    private List<Text> _playerTextList;

    private void AddPlayerText(PhotonPlayer newPlayer)
    {
        Text playerNameText = Instantiate(PlayerTextPrefab);
        playerNameText.transform.SetParent(_parent);
        playerNameText.text = newPlayer.NickName;
        playerNameText.name = newPlayer.NickName;

        _playerTextList.Add(playerNameText);
    }

    private void RemovePlayerText(PhotonPlayer leavingPlayer)
    {
        Text playerNameText = _playerTextList.First(p => p.name == leavingPlayer.NickName);

        _playerTextList.Remove(playerNameText);
        Destroy(playerNameText);
    }

    private void InitPlayerList()
    {
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            AddPlayerText(player);
        }
    }

    void Start()
    {
        _parent = verticalLayoutGroup.GetComponent<RectTransform>();
        _playerTextList = new List<Text>();

        InitPlayerList();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting
        AddPlayerText(other);

        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected

            //LoadArena();
        }
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects
        RemovePlayerText(other);

        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerDisonnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected


            //LoadArena();
        }
    }

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() ListView called by PUN. Now this client is in a room.");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void StartGame()
    {
        if(PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.room.IsOpen = false;
            PhotonNetwork.LoadLevel("Main");
        }
        else
        {
            Debug.Log("You are not master!");
        }
    }
}
