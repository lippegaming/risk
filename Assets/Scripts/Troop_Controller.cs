﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Troop_Controller : MonoBehaviour
{
    Country_Controller _country_Controller;
    Player_Controller _owner;
    SpriteRenderer spriteRenderer;
    Text text;
    int _troopCount;

    public int TroopCount { get { return _troopCount; } }
    public Player_Controller Owner { get { return _owner; } }

    //awake is called before anythingelse
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        text = GetComponentInChildren<Text>();
    }

    public void SetTroopCount(int newTroopCount)
    {
        _troopCount = newTroopCount;

        if(text != null)
            text.text = TroopCount.ToString();
    }

    public void SetNewOwner(Player_Controller newPlayer, int troopCount)
    {
        if (_owner != null)
        {
            _owner.Countries.Remove(this._country_Controller);
            newPlayer.Countries.Add(this._country_Controller);
        }

        _owner = newPlayer;

        string spritePath = GetSpriteFromPlayer(newPlayer);

        Debug.Log("SpritePath: " + spritePath);

        spriteRenderer.sprite = Resources.Load<Sprite>(spritePath);
        SetTroopCount(troopCount);
    }

    private static string GetSpriteFromPlayer(Player_Controller newPlayer)
    {
        string spritePath;
        switch (int.Parse(newPlayer.Id))
        {
            case 0:
                spritePath = "Player_Black";
                break;
            case 1:
                spritePath = "Player_Red";
                break;
            case 2:
                spritePath = "Player_Green";
                break;
            case 3:
                spritePath = "Player_Blue";
                break;
            case 4:
                spritePath = "Player_Pink";
                break;
            case 5:
                spritePath = "Player_Yellow";
                break;
            default:
                throw new ArgumentOutOfRangeException("invalid player id");
        }

        return spritePath;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {      
        //if we send information
        if (stream.isWriting)
        {
            Debug.Log("Serialize View: " + _troopCount.ToString());
            stream.SendNext(_troopCount);
            stream.SendNext(Owner.NickName);
            //stream.SendNext(spriteRenderer.sprite);
        }
        //if we get information
        else
        {
            SetTroopCount((int)stream.ReceiveNext());
            string playerNickName = (string)stream.ReceiveNext();

            Debug.Log("Deserialize View: " + _troopCount.ToString());
            //Debug.Log("Deserialize View: " + playerName);

            //if (playerName != null)
             SetNewOwner(Game_Controller.Instance.Players.First(p => p.NickName == playerNickName), TroopCount);

            
            //spriteRenderer.sprite = (Sprite)stream.ReceiveNext();
        }
    }



    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("OnInstantiate");

        //get photonview Component from new instantiated object
        PhotonView photonView = GetComponent<PhotonView>();

        //retrieve name and country gameobject from instantiationData, and set the country as parent
        string countryName = photonView.instantiationData[0] as string;
        string nickName = photonView.instantiationData[1] as string;

        GameObject country = GameObject.Find(countryName);
        transform.SetParent(country.transform);

        //the country need access to information of owner and troop count
        _country_Controller = country.GetComponent<Country_Controller>();
        _country_Controller.Troop_Controller = this;

        SetNewOwner(Game_Controller.Instance.Players.First(p => p.NickName == nickName), 1);
    }
}
