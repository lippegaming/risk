﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public enum GameState
{
    Lobby,
    SelectingCards,
    DistributeTroops,
    SelectAttackingCountry,
    SelectDefendingCountry,
    WaitForAttackerDice,
    WaitForDefenderDice,
    CalculateAttack,
    SelectRegroupingSource,
    SelectRegroupingSink,
    End
}

public class Game_Controller : Photon.PunBehaviour
{

    public int PlayerCount;
    public GameObject WorldMap;
    public Player_Controller PlayerPrefab;
    public Canvas GUI;

    private Button[] _buttonsDice;
    private Button _buttonNextAction;
    private Text _activePlayerText;
    private Text _attackerDiceText;
    private Text _defenderDiceText;
    private Text _availableTroopsText;
    private Text _logText;
    private Card_Controller[] _cardControllers;

    public Text _missionText;

    private GameState _gameState = GameState.SelectingCards;
    private int _conqueredCountries = 0;

    int[] _attackerDice;
    int[] _defenderDice;
    Country_Controller _attackingCountry;
    Country_Controller _defendingCountry;

    private List<Country_Controller> _countries;
    private List<Player_Controller> _players;
    private List<Card> _cards;
    private List<Mission> _missions;

    public List<Mission> Missions { get { return _missions; } }

    public Player_Controller ActivePlayer
    {
        get
        {
            return _players.First(p => p.State == PlayerState.Active);
        }
    }

    //private Player_Controller _localPlayer;
    public Player_Controller LocalPlayer
    {
        get
        {
            return Players.First(p => p.NickName == PhotonNetwork.player.NickName);
        }
    }

    private static Game_Controller instance;
    public static Game_Controller Instance
    {
        get { return instance; }
    }

    public int CurrentCardBonus = 4;

    public GameState State { get { return _gameState; } }

    private List<Continent> _continents;
    public List<Continent> Continents { get { return _continents; } }
    public List<Player_Controller> Players { get { return _players; } }
    public List<Country_Controller> Countries { get { return _countries; } }

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("Do Something");
    }

    void Awake()
    {

        if (instance == null)
        {
            instance = this;

            PhotonNetwork.OnEventCall += OnEvent;

            PlayerCount = PhotonNetwork.playerList.Length;
            _players = new List<Player_Controller>();
            InitGui();

            //Get all countries from the worldmap object
            _countries = new List<Country_Controller>();
            for (int i = 0; i < WorldMap.transform.childCount; i++)
            {
                _countries.Add(WorldMap.transform.GetChild(i).GetComponent<Country_Controller>());
            }
            LoadNeighbors(_countries);

            //init continents
            _continents = new List<Continent>();
            _continents.Add(new Continent("Europe", _countries.Where(c => c.tag == "Europe").ToArray()));
            _continents.Add(new Continent("Asia", _countries.Where(c => c.tag == "Asia").ToArray()));
            _continents.Add(new Continent("Australia", _countries.Where(c => c.tag == "Australia").ToArray()));
            _continents.Add(new Continent("Africa", _countries.Where(c => c.tag == "Africa").ToArray()));
            _continents.Add(new Continent("SouthAmerica", _countries.Where(c => c.tag == "SouthAmerica").ToArray()));
            _continents.Add(new Continent("NorthAmerica", _countries.Where(c => c.tag == "NorthAmerica").ToArray()));

            //init cards
            _cardControllers = GetComponentsInChildren<Card_Controller>();
            _cards = Card.CreateCards(_countries);

            //Init Mission
            _missions = Mission.InitMissions().ToList();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void InitGui()
    {
        _logText = GUI.GetComponentsInChildren<Text>().First(t => t.name == "Log");
        _missionText = GUI.GetComponentsInChildren<Text>().First(t => t.name == "Mission_Description");
        _attackerDiceText = GUI.GetComponentsInChildren<Text>().First(t => t.name == "Attacker_Dice_Text");
        _defenderDiceText = GUI.GetComponentsInChildren<Text>().First(t => t.name == "Defender_Dice_Text");

        _activePlayerText = GUI.GetComponentsInChildren<Text>().First(t => t.name == "Active_Player_Text");
        _availableTroopsText = GUI.GetComponentsInChildren<Text>().First(t => t.name == "Available_Troops_Text");

        _buttonsDice = GUI.GetComponentsInChildren<Button>().Where(b => b.tag != "Untagged").ToArray();
        _buttonNextAction = GUI.GetComponentsInChildren<Button>().First(b => b.name == "NextAction");

        foreach (var button in _buttonsDice)
        {
            button.interactable = false;
            var colorBlock = button.colors;
            colorBlock.normalColor = new Color32(255, 92, 92, 255);
            colorBlock.highlightedColor = Color.red;
            colorBlock.disabledColor = Color.gray;
            button.colors = colorBlock;
        }
    }

    public void SetCardGui(List<Card> cardsToShow)
    {
        //first reset all cards to avoid inconsistent gui
        foreach (var item in _cardControllers)
        {
            item.ResetGui();
        }

        for (int i = 0; i < cardsToShow.Count; i++)
        {
            _cardControllers[i].SetCard(cardsToShow[i]);
        }
    }

    // Use this for initialization
    void Start()
    {
        if (PhotonNetwork.isMasterClient)
        {
            //Create List for each player
            List<List<Country_Controller>> countriesPerPlayer = new List<List<Country_Controller>>();
            for (int i = 0; i < PlayerCount; i++)
            {
                countriesPerPlayer.Add(new List<Country_Controller>());
            }

            //Add countries randomly to players
            int playerId = 0;
            List<Country_Controller> copiedList = new List<Country_Controller>(_countries);
            for (int i = 0; i < WorldMap.transform.childCount; i++)
            {
                int randomCountryNumber = UnityEngine.Random.Range(0, copiedList.Count - 1);
                Country_Controller randomCountry = copiedList[randomCountryNumber];

                countriesPerPlayer[playerId].Add(randomCountry);
                copiedList.RemoveAt(randomCountryNumber);

                playerId++;
                if (playerId >= PlayerCount)
                {
                    playerId = 0;
                }
            }

                var missionsToGive = _missions.ToList();
            //IMPORTANT
            //Instantiate prefabs not earlier than start()!!!!
            for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
            {
                var mission = missionsToGive[UnityEngine.Random.Range(0, missionsToGive.Count)];
                missionsToGive.Remove(mission);

                List<string> countryNames = new List<string>();
                foreach (var country in countriesPerPlayer[i])
                {
                    countryNames.Add(country.name);
                }

                PhotonPlayer player = PhotonNetwork.playerList[i];
                PhotonNetwork.InstantiateSceneObject("Player_Prefab", Vector3.zero, Quaternion.identity, 0, new object[] { player.NickName, i.ToString(), mission.Description, JsonConvert.SerializeObject(countryNames) });

                //create all troopcountrollers
                //troopcontroller add itself to the country
                foreach (var country in countriesPerPlayer[i])
                {
                    var _troop_Controller = PhotonNetwork.InstantiateSceneObject(
                        "Troop_Prefab",
                        country.transform.position,
                        country.transform.rotation,
                        0,
                        new object[] { country.name, player.NickName});
                }
            }

            Debug.Log("Player Created: " + GameObject.FindObjectsOfType<Player_Controller>().Count());


            LocalPlayer.State = PlayerState.Active;
        }

        _gameState = GameState.SelectingCards;
    }

    internal void StartFight(Country_Controller attackingCountry, Country_Controller defendingCountry)
    {
        Debug.Log("Fight started");
        _attackingCountry = attackingCountry;
        _defendingCountry = defendingCountry;

        //sync

        //show Fighting GUI
        ChangeGameState(GameState.WaitForAttackerDice);


        //wait for attacker to select

        //wait for defender to select

        //result
    }

    private void OnEvent(byte eventcode, object content, int senderid)
    {
        if(eventcode == 0)
        {
            ChangeGameState((GameState)content);
        }
    }

    public void ChangeGameState(GameState newState)
    {
        Debug.Log(newState);
        //Debug.Log(LocalPlayer.State);

        _gameState = newState;

        if (PhotonNetwork.isMasterClient)
        {

            PhotonNetwork.RaiseEvent(0, (int)newState, true, RaiseEventOptions.Default);
        }

        //check if any mission is accomplished
        //is only checked by master
        if (newState != GameState.End)
        {
            if (PhotonNetwork.isMasterClient)
            {
                if(Players.Any(p => p.Mission.Status(p)))
                {
                    _activePlayerText.text = Players.First(p => p.Mission.Status(p)).NickName + " won!";
                    ChangeGameState(GameState.End);
                }
            }               
        }
        else
        {
            _buttonNextAction.interactable = false;
            foreach (var button in _buttonsDice)
            {
                button.interactable = false;
            }
            return;
        }

        if(newState == GameState.DistributeTroops && LocalPlayer.State == PlayerState.Active)
        {
            LocalPlayer.AvailableTroops = CalculateAvailableTroops();
        }
        else if (newState == GameState.WaitForAttackerDice)
        {
            foreach (var button in _buttonsDice.Where(b => b.tag == "AttackerDie"))
            {
                button.interactable = true;
            }
        }
        else if(newState == GameState.WaitForDefenderDice)
        {
            foreach (var button in _buttonsDice.Where(b => b.tag == "AttackerDie"))
            {
                button.interactable = false;
            }
            foreach (var button in _buttonsDice.Where(b => b.tag == "DefenderDie"))
            {
                button.interactable = true;
            }
        }
        else if(newState == GameState.CalculateAttack)
        {
            foreach (var button in _buttonsDice.Where(b => b.tag == "DefenderDie"))
            {
                button.interactable = false;
            }

            int attackerLoss;
            int defenderLoss;
            Calculate(out attackerLoss, out defenderLoss);

            _defendingCountry.Troop_Controller.SetTroopCount(_defendingCountry.Troop_Controller.TroopCount - defenderLoss);
            _attackingCountry.Troop_Controller.SetTroopCount(_attackingCountry.Troop_Controller.TroopCount - attackerLoss);

            if (_defendingCountry.Troop_Controller.TroopCount <= 0)
            {
                _conqueredCountries++;
                if(_conqueredCountries == 1)
                {
                    //AddCardToPlayer
                    int rnd = UnityEngine.Random.Range(0, _cards.Count);
                    Card randomCard = _cards.ElementAt(rnd);
                    
                    Game_Controller.Instance.LocalPlayer.Cards.Add(randomCard);
                    _cards.Remove(randomCard);
                    SetCardGui(Game_Controller.Instance.LocalPlayer.Cards);
                }

                _defendingCountry.Troop_Controller.SetNewOwner(Game_Controller.Instance.LocalPlayer, _attackerDice.Length);
                _attackingCountry.Troop_Controller.SetTroopCount(_attackingCountry.Troop_Controller.TroopCount - _attackerDice.Length);

                //_defendingCountry.Owner.State = PlayerState.Passiv;
                //Game_Controller.Instance.LocalPlayer.State = PlayerState.SelectingAttackingCountry;

                ChangeGameState(GameState.SelectAttackingCountry);
                _attackingCountry.AntiHighlightCountry();
                _defendingCountry.AntiHighlightCountry();
            }
            else
            {
                if (_attackingCountry.Troop_Controller.TroopCount > 1)
                {
                    ChangeGameState(GameState.WaitForAttackerDice);
                }
                else
                {
                    ChangeGameState(GameState.SelectAttackingCountry);
                    //Game_Controller.Instance.LocalPlayer.State = PlayerState.SelectingAttackingCountry;
                    _attackingCountry.AntiHighlightCountry();
                    _defendingCountry.AntiHighlightCountry();
                }
            }
        }
    }

    private int CalculateAvailableTroops()
    {
        int availableTroops = 0;
        _availableTroopsText.text = "";

        List<Card> cardsToUse = LocalPlayer.Cards.Where(c => c.IsSelected).ToList();
        if (cardsToUse != null && Card.CardsAreTradable(cardsToUse))
        {
            foreach (var card in cardsToUse)
            {
                if (card.Country.Troop_Controller.Owner == this)
                {
                    card.Country.Troop_Controller.SetTroopCount(card.Country.Troop_Controller.TroopCount + 2);
                }

                LocalPlayer.Cards.Remove(card);
            }

            //get troops for cards
            availableTroops = Game_Controller.Instance.CurrentCardBonus;
            Game_Controller.Instance.CurrentCardBonus += 2;
            _availableTroopsText.text += "Card Bonus: + " + availableTroops.ToString() + "\n";
        }

        Game_Controller.Instance.SetCardGui(LocalPlayer.Cards);

        Debug.Log(LocalPlayer.Countries.Count.ToString());
        int i = LocalPlayer.Countries.Count / 3;
        if (i < 3)
        {
            i = 3;
        }
        _availableTroopsText.text += LocalPlayer.Countries.Count.ToString() + " Countries: + " + i.ToString() + "\n";


        foreach (var continent in Game_Controller.Instance.Continents)
        {
            if (continent.Countries.TrueForAll(country => country.Troop_Controller.Owner == this))
            {
                switch (continent.Name)
                {
                    case "Europe":
                        i += 5;
                        _availableTroopsText.text += "Europe: + " + 5 + "\n";
                        break;
                    case "Asia":
                        i += 7;
                        _availableTroopsText.text += "Asia: + " + 7 + "\n";
                        break;
                    case "Africa":
                        i += 3;
                        _availableTroopsText.text += "Africa: + " + 3 + "\n";
                        break;
                    case "Australia":
                        i += 2;
                        _availableTroopsText.text += "Australia: + " + 2 + "\n";
                        break;
                    case "NorthAmerica":
                        i += 5;
                        _availableTroopsText.text += "North America: + " + 5 + "\n";
                        break;
                    case "SouthAmerica":
                        i += 2;
                        _availableTroopsText.text += "South America: + " + 2 + "\n";
                        break;

                    default:
                        break;
                }
            }
        }

        availableTroops = i + availableTroops;
        _availableTroopsText.text += "\nAvailable Troops: " + availableTroops;
        return availableTroops;
    }

    private int[] ThrowDice(int diceNumber)
    {
        int[] diceNumbers = new int[diceNumber];

        for (int i = 0; i < diceNumber; i++)
        {
            diceNumbers[i] = UnityEngine.Random.Range(1, 7);
        }

        return diceNumbers;
    }

    private void PrintAttackerDiceResult()
    {
        _attackerDiceText.text = string.Empty;
        foreach (var die in _attackerDice)
        {
            _attackerDiceText.text += die.ToString() + " ";
        }
    }

    private void PrintDefenderDiceResult()
    {
        _defenderDiceText.text = string.Empty;
        foreach (var die in _defenderDice)
        {
            _defenderDiceText.text += die.ToString() + " ";
        }
    }

    public void OnButtonAttackWith1Click()
    {
        Debug.Log("Attack with 1");
        _attackerDice = ThrowDice(1);
        PrintAttackerDiceResult();
        ChangeGameState(GameState.WaitForDefenderDice);       
    }

    public void OnButtonAttackWith2Click()
    {
        if (_attackingCountry.Troop_Controller.TroopCount >= 3)
        {
            Debug.Log("Attack with 2");
            _attackerDice = ThrowDice(2);
            PrintAttackerDiceResult();
            ChangeGameState(GameState.WaitForDefenderDice);
        }
        else
        {
            Debug.Log("Not Enough Troops");
        }
    }

    public void OnButtonAttackWith3Click()
    {      
        if (_attackingCountry.Troop_Controller.TroopCount >= 4)
        {
            Debug.Log("Attack with 3");
            _attackerDice = ThrowDice(3);
            PrintAttackerDiceResult();
            ChangeGameState(GameState.WaitForDefenderDice);
        }
        else
        {
            Debug.Log("Not Enough Troops");
        }
    }

    public void OnButtonDefendWith2Click()
    {
        if (_defendingCountry.Troop_Controller.TroopCount >= 2)
        {
            Debug.Log("Defend with 2");
            _defenderDice = ThrowDice(2);
            PrintDefenderDiceResult();
            ChangeGameState(GameState.CalculateAttack);
        }
        else
        {
            Debug.Log("Not Enough Troops");
        }
    }

    public void OnButtonDefendWith1Click()
    {
        Debug.Log("Defend with 1");
        _defenderDice = ThrowDice(1);
        PrintDefenderDiceResult();
        ChangeGameState(GameState.CalculateAttack);
    }
    public void OnButtonNextAction()
    {
        if(_gameState == GameState.SelectingCards && LocalPlayer.State == PlayerState.Active)
        {
            //LocalPlayer.State = PlayerState.SelectingCountryForTroops;
            ChangeGameState(GameState.DistributeTroops);
            _buttonNextAction.GetComponentInChildren<Text>().text = "Go Regrouping";
        }
        else if(_gameState == GameState.SelectAttackingCountry && LocalPlayer.State == PlayerState.Active)
        {
            //LocalPlayer.State = PlayerState.SelectReordering;
            ChangeGameState(GameState.SelectRegroupingSource);
            _buttonNextAction.GetComponentInChildren<Text>().text = "End Round";
        }
        else if (_gameState == GameState.SelectRegroupingSource && LocalPlayer.State == PlayerState.Active)
        {
            //TODO not working for networking
            int indexOfActivePlayer = _players.IndexOf(ActivePlayer);
            int indexOfNextPlayer = indexOfActivePlayer + 1  >= Players.Count ? 0 : indexOfActivePlayer + 1;           

            //TODO
            //LocalPlayer.State = PlayerState.Passiv;
            //LocalPlayer = _players[indexOfNextPlayer >= _players.Count ? 0 : indexOfNextPlayer];
            //LocalPlayer.State = PlayerState.Active;

            _conqueredCountries = 0;
            _activePlayerText.text = LocalPlayer.NickName;
            _buttonNextAction.GetComponentInChildren<Text>().text = "Distribute Troops";

            LocalPlayer.State = PlayerState.Passiv;
            Players[indexOfNextPlayer].State = PlayerState.Active;
            ChangeGameState(GameState.SelectingCards);
        }
    }

    private void Calculate(out int attackerLoss, out int defenderLoss )
    {
        attackerLoss = 0;
        defenderLoss = 0;

        if(_defenderDice.Length == 1 || _attackerDice.Length == 1)
        {
            var highestAttacker = Mathf.Max(_attackerDice);
            var highestDefender = Mathf.Max(_defenderDice);

            if(highestAttacker > highestDefender)
            {
                defenderLoss++;
            }
            else
            {
                attackerLoss++;
            }
        }
        else
        {           
            var orderedAttackerDice = _attackerDice.OrderByDescending(i => i);
            var orderedDefenderDice = _defenderDice.OrderByDescending(i => i);

            int highestAttacker1 = orderedAttackerDice.ElementAt(0);
            int highestDefender1 = orderedDefenderDice.ElementAt(0);

            if (highestAttacker1 > highestDefender1)
            {
                defenderLoss++;
            }
            else
            {
                attackerLoss++;
            }

            int highestAttacker2 = orderedAttackerDice.ElementAt(1);
            int highestDefender2 = orderedDefenderDice.ElementAt(1);

            if (highestAttacker2 > highestDefender2)
            {
                defenderLoss++;
            }
            else
            {
                attackerLoss++;
            }
        }

        Debug.Log("attacker loss = " + attackerLoss.ToString());
        Debug.Log("defender loss = " + defenderLoss.ToString());
    }

    private void LoadNeighbors(List<Country_Controller> countries)
    {
        foreach (var country in countries)
        {
            var list = new List<Country_Controller>();

            switch (country.name)
            {
                case "Greenland":
                    list.AddRange(countries.Where(c => c.name == "Island" || c.name == "Quebec" || c.name == "North_West_Territories" || c.name == "Ontario"));
                    break;
                case "North_West_Territories":
                    list.AddRange(countries.Where(c => c.name == "Alaska" || c.name == "Alberta" || c.name == "Ontario" || c.name == "Greenland"));
                    break;
                case "Alaska":
                    list.AddRange(countries.Where(c => c.name == "North_West_Territories" || c.name == "Alberta" || c.name == "Kamchatca"));
                    break;
                case "Alberta":
                    list.AddRange(countries.Where(c => c.name == "Alaska" || c.name == "North_West_Territories" || c.name == "Ontario" || c.name == "Western_States"));
                    break;
                case "Ontario":
                    list.AddRange(countries.Where(c => c.name == "Alberta" || c.name == "Greenland" || c.name == "North_West_Territories" || c.name == "Quebec" || c.name == "Western_States" || c.name == "Eastern_States"));
                    break;
                case "Quebec":
                    list.AddRange(countries.Where(c => c.name == "Greenland" || c.name == "Ontario" || c.name == "Eastern_States"));
                    break;
                case "Eastern_States":
                    list.AddRange(countries.Where(c => c.name == "Quebec" || c.name == "Ontario" || c.name == "Central_America" || c.name == "Western_States"));
                    break;
                case "Western_States":
                    list.AddRange(countries.Where(c => c.name == "Alberta" || c.name == "Ontario" || c.name == "Central_America" || c.name == "Eastern_States"));
                    break;
                case "Central_America":
                    list.AddRange(countries.Where(c => c.name == "Venezuela" || c.name == "Western_States" || c.name == "Eastern_States"));
                    break;


                case "Venezuela":
                    list.AddRange(countries.Where(c => c.name == "Central_America" || c.name == "Peru" || c.name == "Brazil"));
                    break;
                case "Peru":
                    list.AddRange(countries.Where(c => c.name == "Venezuela" || c.name == "Argentina" || c.name == "Brazil"));
                    break;
                case "Argentina":
                    list.AddRange(countries.Where(c => c.name == "Peru" || c.name == "Brazil"));
                    break;
                case "Brazil":
                    list.AddRange(countries.Where(c => c.name == "Venezuela" || c.name == "Peru" || c.name == "North_Africa" || c.name == "Argentina"));
                    break;


                case "South_Africa":
                    list.AddRange(countries.Where(c => c.name == "Congo" || c.name == "Madagascar" || c.name == "Eastern_Africa"));
                    break;
                case "Congo":
                    list.AddRange(countries.Where(c => c.name == "South_Africa" || c.name == "North_Africa" || c.name == "Eastern_Africa"));
                    break;
                case "Madagascar":
                    list.AddRange(countries.Where(c => c.name == "South_Africa" || c.name == "Eastern_Africa"));
                    break;
                case "Eastern_Africa":
                    list.AddRange(countries.Where(c => c.name == "South_Africa" || c.name == "North_Africa" || c.name == "Congo" || c.name == "Egypt" || c.name == "Middle_East" || c.name == "Madagascar"));
                    break;
                case "North_Africa":
                    list.AddRange(countries.Where(c => c.name == "Brazil" || c.name == "Southern_Europe" || c.name == "Western_Europe" || c.name == "Congo" || c.name == "Egypt" || c.name == "Eastern_Africa"));
                    break;
                case "Egypt":
                    list.AddRange(countries.Where(c => c.name == "Eastern_Africa" || c.name == "North_Africa" || c.name == "Southern_Europe" || c.name == "Middle_East"));
                    break;


                case "Southern_Europe":
                    list.AddRange(countries.Where(c => c.name == "Egypt" || c.name == "North_Africa" || c.name == "Western_Europe" || c.name == "Middle_East" || c.name == "Northern_Europe" || c.name == "Ukraine"));
                    break;
                case "Western_Europe":
                    list.AddRange(countries.Where(c => c.name == "North_Africa" || c.name == "Southern_Europe" || c.name == "Great_Britain" || c.name == "Northern_Europe"));
                    break;
                case "Great_Britain":
                    list.AddRange(countries.Where(c => c.name == "Island" || c.name == "Western_Europe" || c.name == "Scandinavia" || c.name == "Northern_Europe"));
                    break;
                case "Northern_Europe":
                    list.AddRange(countries.Where(c => c.name == "Western_Europe" || c.name == "Southern_Europe" || c.name == "Great_Britain" || c.name == "Scandinavia" || c.name == "Ukraine"));
                    break;
                case "Ukraine":
                    list.AddRange(countries.Where(c => c.name == "Northern_Europe" || c.name == "Southern_Europe" || c.name == "Scandinavia" || c.name == "Middle_East" || c.name == "Afghanistan" || c.name == "Ural"));
                    break;
                case "Scandinavia":
                    list.AddRange(countries.Where(c => c.name == "Ukraine" || c.name == "Northern_Europe" || c.name == "Great_Britain" || c.name == "Island"));
                    break;
                case "Island":
                    list.AddRange(countries.Where(c => c.name == "Scandinavia" || c.name == "Greenland" || c.name == "Great_Britain"));
                    break;


                case "Middle_East":
                    list.AddRange(countries.Where(c => c.name == "Egypt" || c.name == "Eastern_Africa" || c.name == "Southern_Europe" || c.name == "Ukraine" || c.name == "India" || c.name == "Afghanistan"));
                    break;
                case "India":
                    list.AddRange(countries.Where(c => c.name == "Middle_East" || c.name == "Afghanistan" || c.name == "China" || c.name == "Siam"));
                    break;
                case "Siam":
                    list.AddRange(countries.Where(c => c.name == "India" || c.name == "China" || c.name == "Indonesia"));
                    break;
                case "China":
                    list.AddRange(countries.Where(c => c.name == "Mongolia" || c.name == "Siberia" || c.name == "Ural" || c.name == "Afghanistan" || c.name == "India" || c.name == "Siam"));
                    break;
                case "Mongolia":
                    list.AddRange(countries.Where(c => c.name == "China" || c.name == "Siberia" || c.name == "Irkutsk" || c.name == "Kamchatca" || c.name == "Japan"));
                    break;
                case "Japan":
                    list.AddRange(countries.Where(c => c.name == "Mongolia" || c.name == "Kamchatca"));
                    break;
                case "Kamchatca":
                    list.AddRange(countries.Where(c => c.name == "Mongolia" || c.name == "Japan" || c.name == "Irkutsk" || c.name == "Yakutsk" || c.name == "Alaska"));
                    break;
                case "Irkutsk":
                    list.AddRange(countries.Where(c => c.name == "Mongolia" || c.name == "Kamchatca" || c.name == "Siberia" || c.name == "Yakutsk"));
                    break;
                case "Yakutsk":
                    list.AddRange(countries.Where(c => c.name == "Kamchatca" || c.name == "Siberia" || c.name == "Irkutsk"));
                    break;
                case "Siberia":
                    list.AddRange(countries.Where(c => c.name == "Mongolia" || c.name == "China" || c.name == "Irkutsk" || c.name == "Yakutsk" || c.name == "Ural"));
                    break;
                case "Ural":
                    list.AddRange(countries.Where(c => c.name == "Ukraine" || c.name == "China" || c.name == "Afghanistan" || c.name == "Siberia"));
                    break;
                case "Afghanistan":
                    list.AddRange(countries.Where(c => c.name == "Ukraine" || c.name == "China" || c.name == "Ural" || c.name == "Middle_East" || c.name == "India"));
                    break;


                case "Indonesia":
                    list.AddRange(countries.Where(c => c.name == "New_Guinea" || c.name == "Western_Australia" || c.name == "Siam"));
                    break;
                case "New_Guinea":
                    list.AddRange(countries.Where(c => c.name == "Indonesia" || c.name == "Easterm_Australia" || c.name == "Western_Australia"));
                    break;
                case "Western_Australia":
                    list.AddRange(countries.Where(c => c.name == "New_Guinea" || c.name == "Indonesia" || c.name == "Easterm_Australia"));
                    break;
                case "Easterm_Australia":
                    list.AddRange(countries.Where(c => c.name == "New_Guinea" || c.name == "Western_Australia"));
                    break;

                default:
                    break;
                    //throw new Exception("false country name");
            }

            country.SetNeighbors(list);
        }
    }

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting


        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected


            //LoadArena();
        }
    }


    public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects


        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerDisonnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected


            //LoadArena();
        }
    }
}
