﻿using System.Collections.Generic;

public class Continent
{
    private List<Country_Controller> _countries;
    private string _name;

    public List<Country_Controller> Countries { get { return _countries; } }
    public string Name { get { return _name; } }

    public Continent(string name, params Country_Controller[] countries)
    {
        _countries = new List<Country_Controller>(countries);
        _name = name;
    }
}