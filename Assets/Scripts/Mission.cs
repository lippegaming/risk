﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Mission
{
    Func<Player_Controller, bool> _checkStatus;
    string _description;

    public string Description
    {
        get { return _description; }
    }

    public bool Status (Player_Controller owner)
    {
        return _checkStatus(owner);       
    }

    public Mission(string description, Func<Player_Controller, bool> checkStatus)
    {
        _description = description;
        _checkStatus = checkStatus;
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static IEnumerable<Mission> InitMissions()
    {
        List<Mission> missions = new List<Mission>();

        Func<Player_Controller, bool> Free24Countries = new Func<Player_Controller, bool>((player) =>
        {
            return CheckFor24Countries(player);
        });

        Func<Player_Controller, bool> Free18CountriesWith2Units = new Func<Player_Controller, bool>((player) =>
        {
            if (player.Countries.Where(c => c.Troop_Controller.TroopCount >= 2).Count() >= 18)
            {
                return true;
            }
            else
            {
                return false;
            }
        });

        Func<Player_Controller, bool> FreeAfricaAndNorthAmerica = new Func<Player_Controller, bool>((player) =>
        {
            //cache all countries from africa and north america
            List<Country_Controller> africaAndNorthAmerica = new List<Country_Controller>();
            foreach (var continent in Game_Controller.Instance.Continents.Where(c => c.Name == "Africa" || c.Name == "NorthAmerica"))
            {
                foreach (var country in continent.Countries)
                {
                    africaAndNorthAmerica.Add(country);
                }
            }

            //check if all countries are owned by the player
            if (africaAndNorthAmerica.TrueForAll(country => country.Troop_Controller.Owner == player))
            {
                return true;
            }
            else
            {
                return false;
            }
        });

        Func<Player_Controller, bool> DefeatPlayerBlack = new Func<Player_Controller, bool>((player) =>
        {
            if(Game_Controller.Instance.Players.First(p => p.Color == Color.black) == player)
            {               
                return CheckFor24Countries(player);
            }
            else if(Game_Controller.Instance.Players.First(p => p.Color == Color.black).TroopCount == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        });

        missions.Add(new Mission("Free 24 Countries", Free24Countries));
        missions.Add(new Mission("Free 18 Countries with 2 units each", Free18CountriesWith2Units));
        missions.Add(new Mission("Free North America and Africa", FreeAfricaAndNorthAmerica));
        missions.Add(new Mission("Defeat Player Black. If you are Player Black, free 24 Countries", DefeatPlayerBlack));

        return missions;
    }

    private static bool CheckFor24Countries(Player_Controller player)
    {
        if (player.Countries.Count >= 24)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
