﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public enum CardType
{
    Canon,
    Horse,
    Soldier
}

public class Card
{
    Country_Controller _country;
    CardType _type;
    string _spritePath;

    public bool IsSelected
    {
        get;
        set;
    }

    public Country_Controller Country { get { return _country; } }
    public CardType Type { get { return _type; } }
    public string SpritePath { get { return _spritePath; } }

    public Card(Country_Controller country, CardType type, string spritePath)
    {
        _country = country;
        _type = type;
        _spritePath = spritePath;
    }

    public static List<Card> CreateCards(IEnumerable<Country_Controller> countries)
    {
        List<Card> cards = new List<Card>();

        TextAsset textAsset = (TextAsset)Resources.Load("CardConfig");

        var lines = textAsset.text.Split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var line in lines)
        {
            var lineSplit = line.Split(',');

            cards.Add(new Card(
                countries.First(c => c.name == lineSplit[0]),
                (CardType)Enum.Parse(typeof(CardType), lineSplit[1]),
                lineSplit[0] + "_Card"
            ));
        }

        return cards;
    }

    public static bool CardsAreTradable(List<Card> cards)
    {
        //only if there are 3 selected cards
        if(cards.Count == 3)
        {
            //if all cards have same type
            if (cards.TrueForAll(c => c.Type == CardType.Canon) || cards.TrueForAll(c => c.Type == CardType.Horse) || cards.TrueForAll(c => c.Type == CardType.Soldier))
                return true;

            //if all cards have different type
            if (cards[0].Type != cards[1].Type && cards[0].Type != cards[2].Type && cards[1].Type != cards[2].Type)
                return true;
        }

        return false;
    }
}

