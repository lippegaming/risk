﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card_Controller : MonoBehaviour {

    Sprite _startSprite;
    SpriteRenderer _spriteRenderer;
    Card _card;

	// Use this for initialization
	void Awake () {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _startSprite = _spriteRenderer.sprite;
	}
	 
    public void SetCard(Card newCard)
    {
        _card = newCard;
        _spriteRenderer.sprite = Resources.Load<Sprite>(newCard.SpritePath);
    }

    public void ResetGui()
    {
        _spriteRenderer.sprite = _startSprite;
        _spriteRenderer.color = Color.white;
        if (_card != null)
        {
            _card.IsSelected = false;
            _card = null;
        }
    }

    void OnMouseEnter()
    {
        if(Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectingCards)
        {
            if(_card != null && !_card.IsSelected)
                _spriteRenderer.color = Color.red;
        }
    }

    void OnMouseExit()
    {
        if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectingCards)
        {
            if (_card != null && !_card.IsSelected)
                _spriteRenderer.color = Color.white;
        }
    }

    void OnMouseDown()
    {
        if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectingCards)
        {
            if (_card != null)
            {
                _card.IsSelected = !_card.IsSelected;

                if (_card.IsSelected)
                    _spriteRenderer.color = Color.green;
                else
                    _spriteRenderer.color = Color.red;
            }
        }
    }
}
