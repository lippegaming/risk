﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerState
{
    Active,
    Passiv
}

public class Player_Controller : MonoBehaviour
{
    private string _id;
    private Color _color;
    private string _nickName;
    private Mission _misson;
    private List<Card> _cards = new List<Card>();
    private List<Country_Controller> _owningCountries = new List<Country_Controller>();
    private PlayerState _state = PlayerState.Passiv;

    public Text _availableTroopsText;
    private int _availableTroops = 0;

    public List<Card> Cards
    {
        get { return _cards; }
    }

    public int AvailableTroops
    {
        get { return _availableTroops; }
        set { _availableTroops = value; }
    }

    public List<Country_Controller> Countries { get { return _owningCountries; } }

    public string Id
    {
        get { return _id; }
    }

    public string NickName
    {
        get { return _nickName; }
    }

    public Color Color
    {
        get { return _color; }
    }

    public Mission Mission
    {
        get { return _misson; }
    }

    public int TroopCount
    {
        get
        {
            int count = 0;
            foreach (var country in _owningCountries)
            {
                count += country.Troop_Controller.TroopCount;
            }
            return count;
        }
    }

    public void DecrementAvailableTroops()
    {
        _availableTroops--;
        Debug.Log("DecrementTroops at: " + NickName + " new count: " + _availableTroops.ToString());
        //_availableTroopsText.text = "Available Troops: " + AvailableTroops;
    }

    public PlayerState State
    {
        get { return _state; }
        set
        {
            if (value == PlayerState.Active)
            {
                //show mission
                Game_Controller.Instance._missionText.text = _misson.Description;

                //show cards
                Game_Controller.Instance.SetCardGui(Cards);
            }

            _state = value;
        }
    }

    public void Init(string id, string name, List<Country_Controller> owningCountries, /*Text availableTroopsText,*/ Mission mission)
    {
        _id = id;
        _misson = mission;
        _owningCountries = owningCountries;
        //_availableTroopsText = availableTroopsText;
        _nickName = name;

        switch (int.Parse(id))
        {
            case 0:
                _color = Color.black;                
                break;
            case 1:
                _color = Color.red;
                break;
            case 2:
                _color = Color.green;
                break;
            case 3:
                _color = Color.blue;
                break;
            case 4:
                _color = Color.magenta;
                break;
            case 5:
                _color = Color.yellow;
                break;
            default:
                throw new ArgumentOutOfRangeException("invalid player id");
        }

        //foreach (var country in _owningCountries)
        //{
        //    country.Troop_Controller.SetNewOwner(this, 1, true);
        //}
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            Debug.Log("Serialize Player");
            stream.SendNext((int)State);
        }
        //if we get information
        else
        {
            State = (PlayerState)stream.ReceiveNext();
        }
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("OnInstantiate Player");

        //get photonview Component from new instantiated object
        PhotonView photonView = GetComponent<PhotonView>();

        //retrieve name and country gameobject from instantiationData, and set the country as parent
        string name = photonView.instantiationData[0] as string;
        string id = photonView.instantiationData[1] as string;

        Mission mission = Game_Controller.Instance.Missions.First(m => m.Description == (photonView.instantiationData[2] as string));
        List<string> countryNames = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(photonView.instantiationData[3] as string);

        Debug.Log("Name: " + name);
        Debug.Log("Id: " + id);
        Debug.Log("Mission: " + mission.Description);
        Debug.Log("Countries: ");
        List<Country_Controller> countries = new List<Country_Controller>();
        foreach (var n in countryNames)
        {
            countries.Add(Game_Controller.Instance.Countries.First(c => c.name == n));
            Debug.Log(n);
        }

        Init(id, name, countries, mission);

        //Add itsself to the global player list
        Game_Controller.Instance.Players.Add(this);
    }
}
