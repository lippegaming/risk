﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Country_Controller: MonoBehaviour {
    private Color _startcolor;
    private SpriteRenderer _spriteRenderer;

    private List<Country_Controller> _neighborCountries;
    private bool _isHighlighted;
    private GameObject _troop_Controller;

    static List<Country_Controller> _reachableCountries;
    static Country_Controller _reorderSource;

    public Troop_Controller Troop_Controller;
    public Text GuiText;




    //public int TroopCount { get { return Troop_Controller.TroopCount; } }
    //private Player_Controller _owner;
    //public Player_Controller Owner { get { return _owner; } }

    //public void SetTroopCount(int newTroopCount)
    //{
    //    Troop_Controller.SetTroopCount(newTroopCount);
    //}

    //public void SetNewOwner(Player_Controller newPlayer, int troopCount, bool firstOwner = false)
    //{
    //    if (!firstOwner)
    //    {
    //        _owner.Countries.Remove(this);
    //        newPlayer.Countries.Add(this);
    //    }

    //    _owner = newPlayer;

    //    string spritePath = GetSpriteFromPlayer(newPlayer);

    //    Troop_Controller.SetNewOwner(Resources.Load<Sprite>(spritePath), troopCount);
    //}


    public ReadOnlyCollection<Country_Controller> Neighbors { get { return _neighborCountries.AsReadOnly(); } }

    public void SetNeighbors(List<Country_Controller> neighbors)
    {
        if(neighbors != null)
        {
            _neighborCountries = neighbors;
        }
        else
        {
            throw new ArgumentNullException("neighbors", "null");
        }
    }

    // Use this for initialization
    void Awake()
    {      
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _startcolor = _spriteRenderer.color;

        PhotonNetwork.OnEventCall += OnEvent;
    }

    void Start()
    {
    }

    void OnMouseEnter()
    {
        if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Passiv || Game_Controller.Instance.State == GameState.DistributeTroops)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                HighlightCountry(Color.green);
            }
            else
            {
                HighlightCountry(Color.red);
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectAttackingCountry)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                HighlightCountry(Color.green);

                var neighborCountriesFromOtherPlayers = Neighbors.Where(c => c.Troop_Controller.Owner != Troop_Controller.Owner);
                foreach (var item in neighborCountriesFromOtherPlayers)
                {
                    item.HighlightCountry(Color.red);
                }
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectDefendingCountry)
        {
            if (Troop_Controller.Owner != Game_Controller.Instance.LocalPlayer && _isHighlighted)
            {
                HighlightCountry(Color.blue);
            }
        }
        else if(Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectRegroupingSource)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                HighlightCountry(Color.green);

                _reachableCountries = Neighbors.Where(c => c.Troop_Controller.Owner == Troop_Controller.Owner).ToList();

                bool foundNextCountry;
                do
                {
                    foundNextCountry = false;
                    for (int i = 0; i < _reachableCountries.Count; i++)
                    {                   
                        foreach (var neighbor in _reachableCountries[i].Neighbors.Where(c => c.Troop_Controller.Owner == Troop_Controller.Owner && c != this))
                        {
                            if (!_reachableCountries.Contains(neighbor))
                            {
                                _reachableCountries.Add(neighbor);
                                foundNextCountry = true;
                            }                           
                        }
                    }
                } while (foundNextCountry == true);

                foreach (var item in _reachableCountries)
                {                  
                    item.HighlightCountry(Color.green);
                }
            }
        }
        ////show informations for this country
        //GuiText.text = name + " " + Troop_Controller.TroopCount.ToString();
    }
    void OnMouseExit()
    {
        if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Passiv || Game_Controller.Instance.State == GameState.DistributeTroops)
        {
            HighlightCountry(_startcolor);
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectAttackingCountry)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                HighlightCountry(_startcolor);

                var neighborCountriesFromOtherPlayers = Neighbors.Where(c => c.Troop_Controller.Owner != Troop_Controller.Owner);
                foreach (var item in neighborCountriesFromOtherPlayers)
                {
                    item.HighlightCountry(_startcolor);
                }
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectDefendingCountry)
        {
            if (Troop_Controller.Owner != Game_Controller.Instance.LocalPlayer && _isHighlighted)
            {
                HighlightCountry(Color.red);
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectRegroupingSource)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                AntiHighlightCountry();

                _reachableCountries = Neighbors.Where(c => c.Troop_Controller.Owner == Troop_Controller.Owner).ToList();

                bool foundNextCountry;
                do
                {
                    foundNextCountry = false;
                    for (int i = 0; i < _reachableCountries.Count; i++)
                    {
                        foreach (var neighbor in _reachableCountries[i].Neighbors.Where(c => c.Troop_Controller.Owner == Troop_Controller.Owner && c != this))
                        {
                            if (!_reachableCountries.Contains(neighbor))
                            {
                                _reachableCountries.Add(neighbor);
                                foundNextCountry = true;
                            }
                        }
                    }
                } while (foundNextCountry == true);

                foreach (var item in _reachableCountries)
                {
                    item.AntiHighlightCountry();
                }
            }
        }
    }
    
    public void AntiHighlightCountry()
    {
        HighlightCountry(_startcolor);
    }

    void HighlightCountry(Color color)
    {
        if(color != _startcolor)
        {
            _isHighlighted = true;
        }
        else
        {
            _isHighlighted = false;
        }          

        _spriteRenderer.color = color;
    }

    private void OnEvent(byte eventcode, object content, int senderid)
    {
        if (eventcode == 1 && name == content as string)
        {
            Troop_Controller.SetTroopCount(Troop_Controller.TroopCount + 1);
            Troop_Controller.Owner.DecrementAvailableTroops();
        }
    }

    void OnMouseDown()
    {
        if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.DistributeTroops)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer && Troop_Controller.Owner.AvailableTroops > 0)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    Troop_Controller.SetTroopCount(Troop_Controller.TroopCount + 1);
                    Troop_Controller.Owner.DecrementAvailableTroops();

                    //TODO we need other gamestates
                    //to check if 18 countries with 2 units
                    //Game_Controller.Instance.ChangeGameState(GameState.Idle);                   
                }
                else
                {
                    PhotonNetwork.RaiseEvent(1, name, true, new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient });
                }

                //TODO later we want to accept selection
                //http://answers.unity3d.com/questions/525893/right-mouse-down.html
                if (Troop_Controller.Owner.AvailableTroops <= 0)
                {
                    Game_Controller.Instance.ChangeGameState(GameState.SelectAttackingCountry);
                    OnMouseEnter();
                }
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectAttackingCountry)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                if (Troop_Controller.TroopCount > 1)
                {
                    //put player in selection mode
                    HighlightCountry(Color.magenta);
                    Game_Controller.Instance.ChangeGameState(GameState.SelectDefendingCountry);
                    Debug.Log("Choose country to invade!");
                }
                else
                {
                    Debug.Log("Not enough troops available");
                }
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectDefendingCountry)
        {
            if (this._isHighlighted)
            {
                if (Troop_Controller.Owner != Game_Controller.Instance.LocalPlayer)
                {
                    //Owner.State = PlayerState.Defending;
                    //Game_Controller.Instance.LocalPlayer.State = PlayerState.Attacking;

                    //deactivate all countries which are not involved
                    Country_Controller attackingCountry = Game_Controller.Instance.LocalPlayer.Countries.FirstOrDefault(c => c._isHighlighted);
                    if (attackingCountry != null)
                    {
                        var neighborCountriesFromAttackingCountryButDefendingCountry = attackingCountry.Neighbors.Where(c => c != this);
                        foreach (var item in neighborCountriesFromAttackingCountryButDefendingCountry)
                        {
                            item.AntiHighlightCountry();
                        }
                        Game_Controller.Instance.StartFight(attackingCountry, this);

                    }
                    else
                    {
                        Debug.Log("No attacker found");
                        foreach (var item in Game_Controller.Instance.LocalPlayer.Countries)
                        {
                            Debug.Log(item.name + " is highlighted = " + item._isHighlighted.ToString());
                        }
                    }                   
                }
                else
                {
                    //Owner.State = PlayerState.SelectingAttackingCountry;
                    Game_Controller.Instance.ChangeGameState(GameState.SelectAttackingCountry);
                    HighlightCountry(Color.green);
                }
            }
        }

        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.WaitForAttackerDice)
        {
            if (this._isHighlighted)
            {
                if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
                {
                    //Owner.State = PlayerState.SelectingAttackingCountry;
                    Game_Controller.Instance.ChangeGameState(GameState.SelectAttackingCountry);

                    Debug.Log(Game_Controller.Instance.LocalPlayer.State.ToString());
                    Debug.Log(Game_Controller.Instance.State.ToString());
                    //recall method to simulate "back" move
                    OnMouseEnter();
                    OnMouseDown();
                }
            }           
        }

        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectRegroupingSource)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer)
            {
                _reorderSource = this;
                HighlightCountry(Color.red);
                //Owner.State = PlayerState.Reordering;
                Game_Controller.Instance.ChangeGameState(GameState.SelectRegroupingSink);
            }
        }
        else if (Game_Controller.Instance.LocalPlayer.State == PlayerState.Active && Game_Controller.Instance.State == GameState.SelectRegroupingSink)
        {
            if (Troop_Controller.Owner == Game_Controller.Instance.LocalPlayer && _reachableCountries.Contains(this))
            {
                if (_reorderSource.Troop_Controller.TroopCount > 1)
                {
                    this.Troop_Controller.SetTroopCount(this.Troop_Controller.TroopCount + 1);
                    _reorderSource.Troop_Controller.SetTroopCount(_reorderSource.Troop_Controller.TroopCount - 1);
                }
            }
            else if(this == _reorderSource)
            {
                //Owner.State = PlayerState.SelectReordering;
                Game_Controller.Instance.ChangeGameState(GameState.SelectRegroupingSource);
                OnMouseEnter();
            }
        }
    }
}
